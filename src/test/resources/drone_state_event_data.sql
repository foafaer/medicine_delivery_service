drop table if exists DRONE_STATUS_EVENT;

CREATE TABLE DRONE_STATUS_EVENT
(
    id               number auto_increment not null
        constraint cons_dronestatus_id primary key,
    version          number,
    event_Date       timestamp,
    battery_capacity number(3, 0),
    serial_number    VARCHAR2,
    state            VARCHAR2(20)
);
commit;

insert into DRONE_STATUS_EVENT (id, version, event_Date, battery_capacity, serial_number, state)
values (1, 0, '2022-09-01T12:00:00', 100, '123', 'IDLE'),
       (2, 0, '2022-08-01T12:00:00', 100, '124', 'IDLE'),
       (3, 0, '2022-10-01T12:00:00', 20, '125', 'IDLE');
commit;