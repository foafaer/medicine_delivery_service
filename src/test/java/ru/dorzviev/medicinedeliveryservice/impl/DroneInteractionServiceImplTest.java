package ru.dorzviev.medicinedeliveryservice.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.entity.Drone;
import ru.dorzviev.medicinedeliveryservice.entity.Medication;
import ru.dorzviev.medicinedeliveryservice.entity.State;
import ru.dorzviev.medicinedeliveryservice.mapper.DroneLoadMapper;
import ru.dorzviev.medicinedeliveryservice.mapper.DroneMapper;
import ru.dorzviev.medicinedeliveryservice.repository.DroneRepository;
import ru.dorzviev.medicinedeliveryservice.repository.MedicationRepository;
import ru.dorzviev.medicinedeliveryservice.service.DroneInteractionServiceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DroneInteractionServiceImplTest {

    @Mock
    private DroneRepository droneRepository;

    @Mock
    private MedicationRepository medicationRepository;

    @Mock
    private DroneMapper droneMapper;

    @Mock
    private DroneLoadMapper droneLoadMapper;

    @InjectMocks
    private DroneInteractionServiceImpl droneInteractionService;


    @Test
    void addNewDrone() {
        final Drone drone = new Drone();
        when(droneMapper.to(any())).thenReturn(drone);
        drone.setId(1L);
        when(droneRepository.save(any())).thenReturn(drone);
        final Long actual = droneInteractionService.addNewDrone(new DroneDto());
        assertEquals(drone.getId(), actual);
        verify(droneRepository).save(any());
        verify(droneMapper).to(any());

    }

    @Test
    void initializeLoading() {
        final Drone drone = new Drone();
        drone.setBatteryCapacity(100);
        when(droneRepository.findFirstBySerialNumber(any())).thenReturn(Optional.of(drone));
        when(droneMapper.from(any())).thenReturn(new DroneDto());
        final DroneDto actual = droneInteractionService.initializeLoading("");
        assertNotNull(actual);
        verify(droneRepository).findFirstBySerialNumber(any());
        verify(droneMapper).from(any());
    }

    @Test
    void update() {
        final Drone drone = new Drone();
        drone.setWeightLimit(200);
        drone.setState(State.LOADING);
        when(droneRepository.findFirstBySerialNumber(any())).thenReturn(Optional.of(drone));
        when(medicationRepository.findFirstByCode(any())).thenReturn(Optional.of(new Medication()));
        final DroneLoadDto droneLoadDto = new DroneLoadDto();
        droneLoadDto.setCount(1);
        final MedicationDto medication = new MedicationDto();
        medication.setWeight(100);
        droneLoadDto.setMedication(medication);
        droneInteractionService.update("", Collections.singletonList(droneLoadDto));
        verify(droneRepository).findFirstBySerialNumber(any());
        verify(medicationRepository).findFirstByCode(any());
    }

    @Test
    void findAllAvailableDrones() {
        when(droneRepository.findAvailableForLoading()).thenReturn(null);
        when(droneMapper.fromList(any())).thenReturn(null);
        final List<DroneDto> actual = droneInteractionService.findAllAvailableDrones();
        assertNull(actual);
        verify(droneRepository).findAvailableForLoading();
        verify(droneMapper).fromList(any());
    }

    @Test
    void getBatteryCapacityLevel() {
        final int capacity = 100;
        when(droneRepository.getBatteryCapacityLevel(any())).thenReturn(capacity);
        final Integer actual = droneInteractionService.getBatteryCapacityLevel("");
        assertEquals(capacity, actual);
    }

    @Test
    void getLoad() {
        when(droneRepository.findFirstBySerialNumber(any())).thenReturn(Optional.of(new Drone()));
        when(droneLoadMapper.fromList(any())).thenReturn(Collections.emptyList());
        final List<DroneLoadDto> load = droneInteractionService.getLoad("");
        assertTrue(load.isEmpty());
        verify(droneRepository).findFirstBySerialNumber(any());
        verify(droneLoadMapper).fromList(any());
    }

    @Test
    void unLoad() {
        final Drone drone = new Drone();
        drone.setDroneLoads(new ArrayList<>());
        when(droneRepository.findFirstBySerialNumber(any())).thenReturn(Optional.of(drone));
        when(droneMapper.from(any())).thenReturn(new DroneDto());
        droneInteractionService.unLoad("");
        verify(droneRepository).findFirstBySerialNumber(any());
        verify(droneMapper).from(any());
    }
}