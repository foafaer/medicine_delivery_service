package ru.dorzviev.medicinedeliveryservice.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.entity.DroneStatusEvent;
import ru.dorzviev.medicinedeliveryservice.mapper.DroneStatusEventMapper;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;
import ru.dorzviev.medicinedeliveryservice.repository.DroneStatusRepository;
import ru.dorzviev.medicinedeliveryservice.service.DroneStatusEventServiceImpl;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DroneStatusEventServiceImplTest {

    @Mock
    private DroneStatusEventMapper eventMapper;

    @Mock
    private DroneStatusRepository droneStatusRepository;

    @InjectMocks
    private DroneStatusEventServiceImpl droneStatusEventService;

    @Test
    void getFilteredSuccess() {
        when(droneStatusRepository.getFiltered(any())).thenReturn(
                Collections.singletonList(new DroneStatusEvent()));
        when(eventMapper.fromList(any())).thenReturn(
                Collections.singletonList(new DroneStatusEventDto()));
        final List<DroneStatusEventDto> actual = droneStatusEventService.getFiltered(
                new DroneStateRequestRecord(null, null, null));
        assertNotNull(actual);
        verify(droneStatusRepository).getFiltered(any());
        verify(eventMapper).fromList(any());
    }
}