package ru.dorzviev.medicinedeliveryservice.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.mapper.MedicationMapper;
import ru.dorzviev.medicinedeliveryservice.repository.MedicationRepository;
import ru.dorzviev.medicinedeliveryservice.service.MedicationInteractionServiceImpl;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicationInteractionServiceImplTest {

    @Mock
    private MedicationRepository medicationRepository;

    @Mock
    private MedicationMapper medicationMapper;

    @InjectMocks
    private MedicationInteractionServiceImpl medicationInteractionService;

    @Test
    void getAll() {
        when(medicationRepository.findAll((Sort) any())).thenReturn(null);
        when(medicationMapper.fromList(any())).thenReturn(null);
        final List<MedicationDto> actual = medicationInteractionService.getAll();
        assertNull(actual);
        verify(medicationRepository).findAll((Sort) any());
        verify(medicationMapper).fromList(any());
    }
}