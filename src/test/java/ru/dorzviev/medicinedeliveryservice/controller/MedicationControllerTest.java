package ru.dorzviev.medicinedeliveryservice.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.facade.MedicationFacade;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicationControllerTest {

    @Mock
    private MedicationFacade medicationFacade;

    @InjectMocks
    private MedicationController medicationController;


    @Test
    void getAll() {
        when(medicationFacade.getAll()).thenReturn(Collections.emptyList());
        final ResponseEntity<List<MedicationDto>> actual = medicationController.getAll();
        assertNotNull(actual.getBody());
        verify(medicationFacade).getAll();
    }
}