drop table if exists DRONELOAD;
drop table if exists MEDICATION;
drop table if exists DRONE;
drop table if exists DRONE_STATUS_EVENT;
DROP SEQUENCE IF EXISTS global_seq;
commit;

CREATE SEQUENCE global_seq
    START WITH 100
    INCREMENT BY 1
    MINVALUE 100;
commit;

CREATE TABLE DRONE
(
    id               bigint auto_increment not null
        constraint cons_drone_id primary key,
    version          number,
    serial_number    VARCHAR2(100),
    model            VARCHAR2(30),
    weight_limit     number(3, 0),
    battery_capacity number(3, 0),
    state            VARCHAR2(20)
);
alter table DRONE
    add constraint ser_num_uniq_constraint unique (serial_number);
create index on DRONE (serial_number);
commit;

CREATE TABLE MEDICATION
(
    id        number auto_increment not null
        constraint cons_medication_id primary key,
    version   number,
    name      VARCHAR2(4000),
    weight    number(3, 0),
    code      VARCHAR2(30),
    image_url VARCHAR2(4000)
);
alter table MEDICATION
    add constraint code_num_uniq_constraint unique (code);
create index on MEDICATION (code);
commit;

CREATE TABLE DRONE_LOAD
(
    id            number auto_increment not null
        constraint cons_droneload_id primary key,
    version       number,
    drone_id      number,
    medication_id number,
    count         number,
    foreign key (drone_id) references DRONE (id),
    foreign key (medication_id) references MEDICATION (id)
);
create index on DRONE_LOAD (drone_id);


CREATE TABLE DRONE_STATUS_EVENT
(
    id               number auto_increment not null
        constraint cons_dronestatus_id primary key,
    version          number,
    event_Date       timestamp,
    battery_capacity number(3, 0),
    serial_number    VARCHAR2,
    state            VARCHAR2(20)
);
commit;
