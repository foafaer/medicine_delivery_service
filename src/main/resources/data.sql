insert into DRONE (id, version, serial_number, model, weight_limit, battery_capacity, state)
VALUES (1, 0, '1', 'Heavyweight', 500, 100, 'IDLE'),
       (2, 0, '2', 'Heavyweight', 300, 100, 'IDLE'),
       (3, 0, '3', 'Heavyweight', 300, 100, 'IDLE'),
       (4, 0, '4', 'Heavyweight', 400, 100, 'IDLE'),
       (5, 0, '5', 'Heavyweight', 400, 100, 'IDLE'),
       (6, 0, '6', 'Heavyweight', 400, 100, 'IDLE'),
       (7, 0, '7', 'Heavyweight', 300, 100, 'IDLE'),
       (8, 0, '8', 'Heavyweight', 500, 100, 'IDLE'),
       (9, 0, '9', 'Heavyweight', 400, 100, 'IDLE'),
       (10, 0, '10', 'Heavyweight', 500, 100, 'IDLE');
commit;
INSERT into MEDICATION (id, version, name, weight, code, image_url)
VALUES (1, 0, 'antibiotic', 10, 'A_10', 'https://images.app.goo.gl/2A4Wg18CRW4k6DQt5'),
       (2, 0, 'antibiotic', 15, 'A_15', 'https://images.app.goo.gl/2A4Wg18CRW4k6DQt5'),
       (3, 0, 'antibiotic', 20, 'A_20', 'https://images.app.goo.gl/2A4Wg18CRW4k6DQt5'),
       (4, 0, 'syrup cough', 100, 'S_100', 'https://images.app.goo.gl/X7CkVKc2HYJb5Ppn7'),
       (5, 0, 'syrup cough', 200, 'S_200', 'https://images.app.goo.gl/X7CkVKc2HYJb5Ppn7');
commit;
