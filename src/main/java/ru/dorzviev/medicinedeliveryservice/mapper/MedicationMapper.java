package ru.dorzviev.medicinedeliveryservice.mapper;

import org.mapstruct.Mapper;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.entity.Medication;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicationMapper {

    Medication to(MedicationDto medicationDto);

    MedicationDto from(Medication medication);

    List<MedicationDto> fromList(List<Medication> medications);
}
