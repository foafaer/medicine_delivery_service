package ru.dorzviev.medicinedeliveryservice.mapper;

import org.mapstruct.Mapper;
import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.entity.DroneStatusEvent;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DroneStatusEventMapper {

    DroneStatusEvent to(DroneStatusEventDto statusEventDto);

    DroneStatusEventDto from(DroneStatusEvent statusEvent);

    List<DroneStatusEventDto> fromList(List<DroneStatusEvent> statusEvents);
}
