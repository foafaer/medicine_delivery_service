package ru.dorzviev.medicinedeliveryservice.mapper;

import org.mapstruct.Mapper;
import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.entity.Drone;

import java.util.List;

@Mapper(componentModel = "spring", imports = MedicationMapper.class)
public interface DroneMapper {

    Drone to(DroneDto droneDto);

    DroneDto from(Drone drone);

    List<DroneDto> fromList(List<Drone> drones);

}
