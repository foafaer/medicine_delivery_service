package ru.dorzviev.medicinedeliveryservice.mapper;

import org.mapstruct.Mapper;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.entity.DroneLoad;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DroneLoadMapper {

    DroneLoad to(DroneLoadDto droneLoadDto);

    DroneLoadDto from(DroneLoad droneLoad);

    List<DroneLoadDto> fromList(List<DroneLoad> droneLoads);
}
