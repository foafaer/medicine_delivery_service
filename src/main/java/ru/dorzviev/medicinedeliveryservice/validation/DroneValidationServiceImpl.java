package ru.dorzviev.medicinedeliveryservice.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.exception.DroneValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class DroneValidationServiceImpl implements DroneValidationService {

    private static final Set<String> models = new HashSet<>();

    static {
        models.add("Lightweight");
        models.add("Middleweight");
        models.add("Cruiserweight");
        models.add("Heavyweight");
    }

    private final Validator validator;

    @Override
    public void validate(DroneDto droneDto) {
        commonValidate(droneDto);
        if (!models.contains(droneDto.getModel())) {
            throw new DroneValidationException(
                    "%s - unexpected value of drone's model".formatted(droneDto.getModel()));
        }
    }

    @Override
    public void validate(List<DroneLoadDto> droneLoadDtoList) throws DroneValidationException {
        commonValidate(droneLoadDtoList);
    }

    private void commonValidate(Object object) {
        Set<ConstraintViolation<Object>> violations;
        try {
            violations = validator.validate(object);
        } catch (Exception e) {
            throw new DroneValidationException(e.getMessage(), e);
        }
        if (violations != null && !violations.isEmpty()) {
            throw new DroneValidationException(violations.stream()
                    .map(error -> String.format("%s %s", error.getPropertyPath().toString(),
                            error.getMessage()))
                    .collect(Collectors.joining(",")));
        }
    }


}
