package ru.dorzviev.medicinedeliveryservice.validation;

import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.exception.DroneValidationException;

import java.util.List;

public interface DroneValidationService {

    /**
     * @param droneDto {@link DroneDto}
     * @throws DroneValidationException -- not valid values
     */
    void validate(DroneDto droneDto) throws DroneValidationException;

    /**
     * @param droneLoadDtoList {@link List<DroneLoadDto>} * @throws DroneValidationException -- not
     *                         valid values
     */
    void validate(List<DroneLoadDto> droneLoadDtoList) throws DroneValidationException;

}
