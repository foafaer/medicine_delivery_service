package ru.dorzviev.medicinedeliveryservice.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import ru.dorzviev.medicinedeliveryservice.exception.DroneNotFoundException;
import ru.dorzviev.medicinedeliveryservice.exception.DroneValidationException;
import ru.dorzviev.medicinedeliveryservice.exception.DroneWrongStateException;
import ru.dorzviev.medicinedeliveryservice.exception.LowBatteryLevelException;
import ru.dorzviev.medicinedeliveryservice.exception.MedicationNotFoundException;
import ru.dorzviev.medicinedeliveryservice.exception.TooHeavyLoadException;


@Slf4j
@Aspect
@Component
public class ExceptionHandlerAnnotationAspect {
    @Around(value = "@annotation(ru.dorzviev.medicinedeliveryservice.aspect.ExceptionHandler)")
    public Object executing(ProceedingJoinPoint proceedingJoinPoint) {
        final String methodName = proceedingJoinPoint.getSignature().getName();
        log.info("{} - method executing", methodName);
        final Object[] args = proceedingJoinPoint.getArgs();
        try {
            return proceedingJoinPoint.proceed(args);
        } catch (DroneValidationException e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        } catch (DroneNotFoundException | MedicationNotFoundException e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        } catch (LowBatteryLevelException e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(-100, e.getMessage(), e);
        } catch (TooHeavyLoadException e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(-101, e.getMessage(), e);
        } catch (DroneWrongStateException e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(-102, e.getMessage(), e);
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }
}
