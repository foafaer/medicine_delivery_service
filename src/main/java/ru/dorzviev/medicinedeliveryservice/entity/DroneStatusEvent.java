package ru.dorzviev.medicinedeliveryservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "DRONE_STATUS_EVENT")
public class DroneStatusEvent extends IdentifiableEntity {


    @Column(name = "serial_number")
    private String serialNumber;
    @Column(name = "event_Date")
    private LocalDateTime eventDate;

    @Column(name = "battery_capacity")
    private Integer batteryCapacity;

    @Column
    private String state;

    @PrePersist
    protected void onCreate() {
        eventDate = LocalDateTime.now();
    }

}
