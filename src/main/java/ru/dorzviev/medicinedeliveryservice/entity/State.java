package ru.dorzviev.medicinedeliveryservice.entity;

import lombok.Getter;

@Getter
public enum State {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING
}
