package ru.dorzviev.medicinedeliveryservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "DRONE_LOAD")
public class DroneLoad extends IdentifiableEntity {

    @ManyToOne
    @JoinColumn(name = "drone_id", nullable = false)
    private Drone drone;

    @Column
    private Integer count;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "medication_id")
    private Medication medication;
}
