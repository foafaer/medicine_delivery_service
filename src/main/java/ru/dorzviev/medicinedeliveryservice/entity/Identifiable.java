package ru.dorzviev.medicinedeliveryservice.entity;

public interface Identifiable {

    long getId();

}
