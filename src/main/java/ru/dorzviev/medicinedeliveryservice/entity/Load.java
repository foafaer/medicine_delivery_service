package ru.dorzviev.medicinedeliveryservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@Getter
@Setter
@MappedSuperclass
public abstract class Load extends IdentifiableEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "imageUrl")
    private String imageUrl;
}
