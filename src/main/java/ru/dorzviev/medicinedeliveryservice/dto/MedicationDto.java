package ru.dorzviev.medicinedeliveryservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MedicationDto {

    @NotNull
    @Pattern(regexp = "^[0-9A-Za-z_-]*$")
    private String name;

    @NotNull
    @Pattern(regexp = "^[0-9A-Z_]*$")
    private String code;

    @NotNull
    private Integer weight;

    private String imageUrl;

}
