package ru.dorzviev.medicinedeliveryservice.service;

import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.exception.DroneNotFoundException;
import ru.dorzviev.medicinedeliveryservice.exception.LowBatteryLevelException;
import ru.dorzviev.medicinedeliveryservice.exception.TooHeavyLoadException;

import java.util.List;

public interface DroneInteractionService {

    /**
     * @param droneDto {@link DroneDto}
     * @return {@link Long}
     */
    Long addNewDrone(final DroneDto droneDto);

    /**
     * @param serialNumber {@link String}
     * @return {@link DroneDto}
     * @throws DroneNotFoundException   -- no record
     * @throws LowBatteryLevelException -- battery capacity is lower then 25%
     */
    DroneDto initializeLoading(final String serialNumber)
            throws DroneNotFoundException, LowBatteryLevelException;

    /**
     * @param serialNumber {@link String}
     * @param droneLoad    {@link List<DroneLoadDto>}
     * @throws DroneNotFoundException -- no record
     * @throws TooHeavyLoadException  -- weight of load is more than of drone's weight limit
     */
    void update(final String serialNumber, final List<DroneLoadDto> droneLoad)
            throws DroneNotFoundException, TooHeavyLoadException;

    /**
     * @return {@link List<DroneLoadDto>}
     */
    List<DroneDto> findAllAvailableDrones();

    /**
     * @param serialNumber {@link String serialNumber}
     * @return {@link Integer}
     */
    Integer getBatteryCapacityLevel(final String serialNumber);

    /**
     * @param serialNumber {@link String}
     * @return {@link List<DroneLoadDto>}
     */
    List<DroneLoadDto> getLoad(final String serialNumber) throws DroneNotFoundException;

    /**
     * @param serialNumber {@link String}
     * @return {@link DroneDto}
     */
    DroneDto unLoad(final String serialNumber);
}
