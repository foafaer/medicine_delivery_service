package ru.dorzviev.medicinedeliveryservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.entity.Drone;
import ru.dorzviev.medicinedeliveryservice.entity.DroneLoad;
import ru.dorzviev.medicinedeliveryservice.entity.State;
import ru.dorzviev.medicinedeliveryservice.exception.DroneNotFoundException;
import ru.dorzviev.medicinedeliveryservice.exception.DroneWrongStateException;
import ru.dorzviev.medicinedeliveryservice.exception.LowBatteryLevelException;
import ru.dorzviev.medicinedeliveryservice.exception.MedicationNotFoundException;
import ru.dorzviev.medicinedeliveryservice.exception.TooHeavyLoadException;
import ru.dorzviev.medicinedeliveryservice.mapper.DroneLoadMapper;
import ru.dorzviev.medicinedeliveryservice.mapper.DroneMapper;
import ru.dorzviev.medicinedeliveryservice.repository.DroneRepository;
import ru.dorzviev.medicinedeliveryservice.repository.MedicationRepository;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
@RequiredArgsConstructor
public class DroneInteractionServiceImpl implements DroneInteractionService {

    private static final int MINIMAL_CAPACITY_FOR_LOADING = 25;

    private final DroneRepository droneRepository;

    private final MedicationRepository medicationRepository;

    private final DroneMapper droneMapper;

    private final DroneLoadMapper droneLoadMapper;

    @Override
    public Long addNewDrone(final DroneDto droneDto) {
        final Drone drone = droneMapper.to(droneDto);
        drone.setState(State.IDLE);
        final Drone saved = droneRepository.save(drone);
        return saved.getId();
    }

    @Override
    public DroneDto initializeLoading(final String serialNumber) {
        final Drone drone = droneRepository.findFirstBySerialNumber(serialNumber)
                .orElseThrow(DroneNotFoundException::new);
        checkBatteryCapacity(drone);
        drone.setState(State.LOADING);
        return droneMapper.from(drone);
    }

    private void checkBatteryCapacity(final Drone drone) {
        if (drone.getBatteryCapacity() < MINIMAL_CAPACITY_FOR_LOADING) {
            throw new LowBatteryLevelException(
                    "Drone's battery capacity - %s is lower then 25, charge it before loading".formatted(
                            drone.getBatteryCapacity()));
        }
    }

    @Override
    public void update(final String serialNumber, final List<DroneLoadDto> droneLoad) {
        final Drone drone = droneRepository.findFirstBySerialNumber(serialNumber)
                .orElseThrow(DroneNotFoundException::new);
        checkDrone(droneLoad, drone);
        List<DroneLoad> droneLoads = fillDroneLoad(droneLoad, drone);
        if (drone.getDroneLoads() == null) {
            drone.setDroneLoads(new ArrayList<>());
        }
        drone.getDroneLoads().clear();
        drone.getDroneLoads().addAll(droneLoads);
        drone.setState(State.LOADED);
    }

    private List<DroneLoad> fillDroneLoad(final List<DroneLoadDto> droneLoad, final Drone drone) {
        List<DroneLoad> droneLoads = new ArrayList<>();
        droneLoad.forEach(droneLoadDto -> {
            final DroneLoad load = new DroneLoad();
            load.setDrone(drone);
            load.setCount(droneLoadDto.getCount());
            load.setMedication(
                    medicationRepository.findFirstByCode(droneLoadDto.getMedication().getCode())
                            .orElseThrow(MedicationNotFoundException::new));
            droneLoads.add(load);
        });
        return droneLoads;
    }

    private void checkDrone(final List<DroneLoadDto> droneLoad, final Drone drone) {
        final int fullWeightOfMedications = droneLoad.stream()
                .mapToInt(m -> m.getMedication().getWeight() * m.getCount()).sum();
        if (fullWeightOfMedications > drone.getWeightLimit()) {
            throw new TooHeavyLoadException(
                    "Weight of loading medications is greater then drone can take: %d > %d".formatted(
                            fullWeightOfMedications, drone.getWeightLimit()));
        }
        if (drone.getState() != State.LOADING) {
            throw new DroneWrongStateException(
                    "To start loading initially you have to set drone to LOADING state!");
        }
    }


    @Override
    public List<DroneDto> findAllAvailableDrones() {
        return droneMapper.fromList(droneRepository.findAvailableForLoading());
    }


    @Override
    public Integer getBatteryCapacityLevel(final String serialNumber) {
        return droneRepository.getBatteryCapacityLevel(serialNumber);
    }

    @Override
    public List<DroneLoadDto> getLoad(final String serialNumber) {
        final Drone drone = droneRepository.findFirstBySerialNumber(serialNumber)
                .orElseThrow(DroneNotFoundException::new);
        return droneLoadMapper.fromList(drone.getDroneLoads());
    }

    @Override
    public DroneDto unLoad(final String serialNumber) {
        final Drone drone = droneRepository.findFirstBySerialNumber(serialNumber)
                .orElseThrow(DroneNotFoundException::new);
        drone.setState(State.IDLE);
        drone.getDroneLoads().clear();
        return droneMapper.from(drone);
    }
}
