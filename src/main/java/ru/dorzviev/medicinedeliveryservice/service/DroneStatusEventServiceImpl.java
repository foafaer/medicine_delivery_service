package ru.dorzviev.medicinedeliveryservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.entity.DroneStatusEvent;
import ru.dorzviev.medicinedeliveryservice.mapper.DroneStatusEventMapper;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;
import ru.dorzviev.medicinedeliveryservice.repository.DroneStatusRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DroneStatusEventServiceImpl implements DroneStatusEventService {

    private final DroneStatusEventMapper eventMapper;

    private final DroneStatusRepository droneStatusRepository;

    @Override
    public List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord) {
        final List<DroneStatusEvent> events = droneStatusRepository.getFiltered(requestRecord);
        return eventMapper.fromList(events);
    }
}
