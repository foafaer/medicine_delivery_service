package ru.dorzviev.medicinedeliveryservice.service;

import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;

import java.util.List;

public interface MedicationInteractionService {

    /**
     * @return {@link List<MedicationDto>}
     */
    List<MedicationDto> getAll();

}
