package ru.dorzviev.medicinedeliveryservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.entity.Drone;
import ru.dorzviev.medicinedeliveryservice.entity.DroneStatusEvent;
import ru.dorzviev.medicinedeliveryservice.repository.DroneRepository;
import ru.dorzviev.medicinedeliveryservice.repository.DroneStatusEventRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduledDroneEventServiceImpl implements ScheduledDroneEventService {

    private final DroneRepository droneRepository;

    private final DroneStatusEventRepository eventRepository;

    @Override
    @Scheduled(initialDelay = 20, fixedDelay = 50, timeUnit = TimeUnit.SECONDS)
    public void checkStatusAndSave() {
        final List<Drone> drones = droneRepository.findAll();
        List<DroneStatusEvent> droneStatusEvents = new ArrayList<>();
        drones.forEach(drone -> {
            final DroneStatusEvent droneStatusEvent = new DroneStatusEvent();
            droneStatusEvent.setSerialNumber(drone.getSerialNumber());
            droneStatusEvent.setState(drone.getState().name());
            droneStatusEvent.setBatteryCapacity(drone.getBatteryCapacity());
            droneStatusEvents.add(droneStatusEvent);
        });
        eventRepository.saveAll(droneStatusEvents);
    }

    @Override
    @Scheduled(initialDelay = 1, fixedDelay = 10, timeUnit = TimeUnit.DAYS)
    public void deleteEvents() {
        log.info("deleting of old records...");
        final LocalDateTime localDateTime = LocalDateTime.now().minusDays(3);
        eventRepository.deleteDroneStatusEventByEventDateBefore(localDateTime);
    }
}

