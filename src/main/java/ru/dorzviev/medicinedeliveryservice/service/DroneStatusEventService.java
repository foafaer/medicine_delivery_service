package ru.dorzviev.medicinedeliveryservice.service;

import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;

import java.util.List;

public interface DroneStatusEventService {

    /**
     * @param requestRecord -- record with filer, sort, pagination data
     * @return {@link List<DroneStatusEventDto>}
     */
    List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord);
}
