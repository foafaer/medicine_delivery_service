package ru.dorzviev.medicinedeliveryservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.entity.Medication;
import ru.dorzviev.medicinedeliveryservice.mapper.MedicationMapper;
import ru.dorzviev.medicinedeliveryservice.repository.MedicationRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicationInteractionServiceImpl implements MedicationInteractionService {

    private final MedicationRepository medicationRepository;

    private final MedicationMapper medicationMapper;

    @Override
    public List<MedicationDto> getAll() {
        final List<Medication> medications = medicationRepository.findAll(Sort.by("name").ascending());
        return medicationMapper.fromList(medications);
    }
}
