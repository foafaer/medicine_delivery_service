package ru.dorzviev.medicinedeliveryservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.facade.DroneInteractionFacade;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/drone")
public class DroneController {
    private final DroneInteractionFacade droneInteractionFacade;

    @PostMapping
    public ResponseEntity<Long> create(@RequestBody DroneDto droneDto) {
        return ResponseEntity.ok(droneInteractionFacade.addNew(droneDto));
    }

    @GetMapping(path = "/{serialNumber}")
    public ResponseEntity<DroneDto> initializeLoading(@PathVariable String serialNumber) {
        return ResponseEntity.ok(droneInteractionFacade.initializeLoading(serialNumber));
    }

    @GetMapping(path = "/load/{serialNumber}")
    public ResponseEntity<List<DroneLoadDto>> getLoad(@PathVariable String serialNumber) {
        return ResponseEntity.ok(droneInteractionFacade.getLoad(serialNumber));
    }

    @DeleteMapping(path = "/load/{serialNumber}")
    public ResponseEntity<DroneDto> unLoad(@PathVariable String serialNumber) {
        droneInteractionFacade.unLoad(serialNumber);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(path = "/{serialNumber}")
    public ResponseEntity update(@PathVariable String serialNumber,
                                 @RequestBody List<DroneLoadDto> droneLoad) {
        droneInteractionFacade.update(serialNumber, droneLoad);
        return ResponseEntity.accepted().build();
    }

    @GetMapping
    public ResponseEntity<List<DroneDto>> getAvailableForLoading() {
        final List<DroneDto> availableForLoading = droneInteractionFacade.getAvailableForLoading();
        return ResponseEntity.ok(availableForLoading);
    }

    @PatchMapping(path = "/{serialNumber}")
    public ResponseEntity<Integer> getBatteryLevel(@PathVariable String serialNumber) {
        return ResponseEntity.ok(droneInteractionFacade.getBatteryCapacityLevel(serialNumber));
    }

}
