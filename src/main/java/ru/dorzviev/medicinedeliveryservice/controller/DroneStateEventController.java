package ru.dorzviev.medicinedeliveryservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.facade.DroneStatusEventFacade;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/event")
public class DroneStateEventController {

    private final DroneStatusEventFacade eventFacade;

    @PostMapping
    public ResponseEntity<List<DroneStatusEventDto>> getFiltered(
            @RequestBody DroneStateRequestRecord requestRecord) {
        return ResponseEntity.ok(eventFacade.getFiltered(requestRecord));
    }

}
