package ru.dorzviev.medicinedeliveryservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.facade.MedicationFacade;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/medication")
public class MedicationController {

    private final MedicationFacade medicationFacade;

    @GetMapping
    public ResponseEntity<List<MedicationDto>> getAll() {
        return ResponseEntity.ok(medicationFacade.getAll());
    }

}
