package ru.dorzviev.medicinedeliveryservice.repository;

import ru.dorzviev.medicinedeliveryservice.entity.DroneStatusEvent;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;

import java.util.List;

public interface DroneStatusRepository {

    List<DroneStatusEvent> getFiltered(DroneStateRequestRecord droneStateRequestRecord);

}
