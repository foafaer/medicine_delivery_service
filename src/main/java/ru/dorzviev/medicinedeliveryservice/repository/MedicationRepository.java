package ru.dorzviev.medicinedeliveryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.dorzviev.medicinedeliveryservice.entity.Medication;

import java.util.Optional;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

    Optional<Medication> findFirstByCode(final String code);

}
