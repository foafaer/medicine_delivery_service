package ru.dorzviev.medicinedeliveryservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.dorzviev.medicinedeliveryservice.entity.DroneStatusEvent;

import java.time.LocalDateTime;

public interface DroneStatusEventRepository extends JpaRepository<DroneStatusEvent, Long> {

    @Transactional
    void deleteDroneStatusEventByEventDateBefore(LocalDateTime localDateTime);
}
