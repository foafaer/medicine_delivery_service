package ru.dorzviev.medicinedeliveryservice.record;

/**
 * Record for drone's status events pagination
 */
public record PaginationRecord(Integer pageNum, Integer pageSize) {

}
