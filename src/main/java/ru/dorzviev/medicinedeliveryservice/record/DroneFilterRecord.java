package ru.dorzviev.medicinedeliveryservice.record;

import java.time.LocalDateTime;

/**
 * Record for drone's status events filtering
 */
public record DroneFilterRecord(LocalDateTime from, LocalDateTime to, String serialNumber) {

}
