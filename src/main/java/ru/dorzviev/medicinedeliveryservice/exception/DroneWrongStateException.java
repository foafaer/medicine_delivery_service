package ru.dorzviev.medicinedeliveryservice.exception;

public class DroneWrongStateException extends RuntimeException {

    public DroneWrongStateException(String message) {
        super(message);
    }
}
