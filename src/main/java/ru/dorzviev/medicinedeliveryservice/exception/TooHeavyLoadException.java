package ru.dorzviev.medicinedeliveryservice.exception;

public class TooHeavyLoadException extends RuntimeException {

    public TooHeavyLoadException(String message) {
        super(message);
    }

}
