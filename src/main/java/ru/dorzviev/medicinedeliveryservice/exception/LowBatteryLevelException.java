package ru.dorzviev.medicinedeliveryservice.exception;

public class LowBatteryLevelException extends RuntimeException {

    public LowBatteryLevelException(String message) {
        super(message);
    }

}
