package ru.dorzviev.medicinedeliveryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicineDeliveryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedicineDeliveryServiceApplication.class, args);
	}

}
