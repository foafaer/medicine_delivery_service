package ru.dorzviev.medicinedeliveryservice.facade;

import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;

import java.util.List;

public interface DroneStatusEventFacade {

    /**
     * Method lets to get drone's status data with sorting, filtering, pagination
     *
     * @return {@link List<DroneStatusEventDto>}
     */
    List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord);

}
