package ru.dorzviev.medicinedeliveryservice.facade;

import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;

import java.util.List;

public interface MedicationFacade {

    /**
     * @return {@link List<MedicationDto>}
     */
    List<MedicationDto> getAll();

}
