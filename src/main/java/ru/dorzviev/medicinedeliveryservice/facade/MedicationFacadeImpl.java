package ru.dorzviev.medicinedeliveryservice.facade;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.aspect.ExceptionHandler;
import ru.dorzviev.medicinedeliveryservice.dto.MedicationDto;
import ru.dorzviev.medicinedeliveryservice.service.MedicationInteractionService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicationFacadeImpl implements MedicationFacade {

    private final MedicationInteractionService medicationInteractionService;

    @Override
    @ExceptionHandler
    public List<MedicationDto> getAll() {
        return medicationInteractionService.getAll();
    }
}
