package ru.dorzviev.medicinedeliveryservice.facade;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.aspect.ExceptionHandler;
import ru.dorzviev.medicinedeliveryservice.dto.DroneStatusEventDto;
import ru.dorzviev.medicinedeliveryservice.record.DroneStateRequestRecord;
import ru.dorzviev.medicinedeliveryservice.service.DroneStatusEventService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DroneStatusEventFacadeImpl implements DroneStatusEventFacade {

    private final DroneStatusEventService droneStatusEventService;

    @Override
    @ExceptionHandler
    public List<DroneStatusEventDto> getFiltered(DroneStateRequestRecord requestRecord) {
        return droneStatusEventService.getFiltered(requestRecord);
    }
}
