package ru.dorzviev.medicinedeliveryservice.facade;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dorzviev.medicinedeliveryservice.aspect.ExceptionHandler;
import ru.dorzviev.medicinedeliveryservice.dto.DroneDto;
import ru.dorzviev.medicinedeliveryservice.dto.DroneLoadDto;
import ru.dorzviev.medicinedeliveryservice.service.DroneInteractionService;
import ru.dorzviev.medicinedeliveryservice.validation.DroneValidationService;

import java.util.List;


@Service
@RequiredArgsConstructor
public class DroneInteractionFacadeImpl implements DroneInteractionFacade {

    private final DroneInteractionService droneInteractionService;

    private final DroneValidationService validationService;

    @Override
    @ExceptionHandler
    public Long addNew(final DroneDto droneDto) {
        validationService.validate(droneDto);
        return droneInteractionService.addNewDrone(droneDto);

    }

    @Override
    @ExceptionHandler
    public DroneDto initializeLoading(final String serialNumber) {
        return droneInteractionService.initializeLoading(serialNumber);
    }

    @Override
    @ExceptionHandler
    public void update(final String serialNumber, final List<DroneLoadDto> droneLoad) {
        if (droneLoad == null || droneLoad.isEmpty()) {
            return;
        }
        validationService.validate(droneLoad);
        droneInteractionService.update(serialNumber, droneLoad);
    }

    @Override
    @ExceptionHandler
    public List<DroneDto> getAvailableForLoading() {
        return droneInteractionService.findAllAvailableDrones();
    }

    @Override
    @ExceptionHandler
    public Integer getBatteryCapacityLevel(final String serialNumber) {
        return droneInteractionService.getBatteryCapacityLevel(serialNumber);
    }

    @Override
    @ExceptionHandler
    public List<DroneLoadDto> getLoad(final String serialNumber) {
        return droneInteractionService.getLoad(serialNumber);
    }

    @Override
    @ExceptionHandler
    public DroneDto unLoad(final String serialNumber) {
        return droneInteractionService.unLoad(serialNumber);
    }
}
